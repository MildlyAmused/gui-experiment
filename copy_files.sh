#!/bin/bash

FROM=$1
TO=$2
if [ $# -eq 2 ]; then
	cp $FROM"_example.h" $TO"_example.h"
	cp $FROM"_example.c" $TO"_example.c"
	cp $FROM"_comp.sh" $TO"_comp.sh"
fi
