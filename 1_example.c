#include <gtk/gtk.h>

#define BUFFER_SIZE 300
GtkBuilder	*builder; 
GtkWidget	*main_window;
GtkWidget	*main_box;
GtkWidget 	*text_area;
GtkWidget 	*sub_box;
GtkWidget 	*text_entry;
GtkWidget 	*send_button;
GtkWidget 	*send_image;

char buffer[BUFFER_SIZE];

int main(int argc, char *argv[]){
	gtk_init(&argc, &argv); // init Gtk
	builder = gtk_builder_new_from_file ("Design.glade");
	main_window = GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));
	g_signal_connect(main_window, "destroy", 
	G_CALLBACK(gtk_main_quit), NULL);
	gtk_builder_connect_signals(builder, NULL);
	main_box = GTK_WIDGET(gtk_builder_get_object(builder, "main_box"));
	text_area = GTK_WIDGET(gtk_builder_get_object(builder, "text_area"));
	sub_box = GTK_WIDGET(gtk_builder_get_object(builder, "sub_box"));
	text_entry = GTK_WIDGET(gtk_builder_get_object(builder, "text_entry"));
	send_button = GTK_WIDGET(gtk_builder_get_object(builder, "send_button"));
	send_image = GTK_WIDGET(gtk_builder_get_object(builder, "send_image"));

	gtk_widget_show(main_window);

	gtk_main();
	/*
	g_object_unref(send_image);
	g_object_unref(send_button);
	g_object_unref(text_entry);
	g_object_unref(sub_box);
	g_object_unref(text_area);
	g_object_unref(main_box);
	g_object_unref(main_window);
	g_object_unref(builder);
	*/
	/*
	if(g_object_is_floating(send_image))
		printf("This send_image is floating\n");
	if(g_object_is_floating(send_button))
		printf("This send_button is floating\n");
	if(g_object_is_floating(text_entry))
		printf("This text_entry is floating\n");
	if(g_object_is_floating(sub_box))
		printf("This sub_box is floating\n");
	if(g_object_is_floating(text_area))
		printf("This text_area is floating\n");
	if(g_object_is_floating(main_box))
		printf("This main_box is floating\n");
	if(g_object_is_floating(main_window))
		printf("This main_window is floating\n");
	if(g_object_is_floating(builder))
		printf("This builder is floating\n");
	*/
	return EXIT_SUCCESS;
}

void on_send_button_clicked(GtkButton *b){
	//gtk_label_set_text(GTK_LABEL(label1), (const gchar* ) "Hello World");
	gtk_editable_delete_text(GTK_EDITABLE (text_entry), 0, -1);
}

void on_text_entry_changed(GtkEntry *e){
	strcpy(buffer, gtk_entry_get_text(e));
	// do something with buffer...
}
