#!/bin/bash
#TYPE=leak
if (( $# == 1 )); then
gcc "$1"_example.c -o "$1"_example -lm `pkg-config --cflags --libs gtk+-3.0` 
else 
gcc "$1"_example.c -o "$1"_example -lm \
`pkg-config --cflags --libs gtk+-3.0` -ggdb -fno-omit-frame-pointer \
-fsanitize=$2 -static-libgcc
fi
