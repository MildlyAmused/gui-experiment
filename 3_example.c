#include <gtk/gtk.h>
#include "3_example.h"

typedef struct pass_data{
	GtkWidget *t_entry;
	GtkWidget *t_area;
}pass_data;

static void activate(GtkApplication *app, GIOChannel *sock_act);
static GtkWidget *create_box(GIOChannel *sock_act);
static GtkWidget *create_scrolled(GIOChannel *sock_act);
static GtkWidget *create_sub_box(GtkWidget *t_area);
void on_s_button_clicked(GtkButton *b, pass_data *entries);
static gboolean net_event(GIOChannel *sock_act, GIOCondition cond, 
gpointer data);
void print_buff(GtkTextView *t_area, const gchar *text_input);
//GtkWidget *find_child(GtkWidget *parent, const gchar *name);

int main(int argc, char *argv[]){
	int status = initialize();
	if(status != -1){
		GtkApplication *app = gtk_application_new
		("com.gitlab.MildlyAmused.example_2", G_APPLICATION_FLAGS_NONE);
		GIOChannel *sock_act = g_io_channel_unix_new(client_socket);
		g_signal_connect(app, "activate", G_CALLBACK(activate), sock_act);
		status = g_application_run(G_APPLICATION(app), argc, argv);
		g_object_unref(app);
		g_io_channel_unref(sock_act);
		g_io_channel_unref(sock_act);
		close(client_socket);
	}
	return status;
}

static gboolean net_event(GIOChannel *sock_act, GIOCondition cond, 
gpointer data){
	//GtkWidget *text_area = GTK_WIDGET(data);
	GtkTextView *text_area = GTK_TEXT_VIEW(data);
	gchar *text = net_check();
	if(text != NULL){
		print_buff(text_area, text);
	}
	return TRUE;
}

static void activate(GtkApplication *app, GIOChannel *sock_act){
	// This is the window being set...
	GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_name(window, "main_window");
	gtk_application_add_window(app, GTK_WINDOW(window));
	gtk_window_set_title(GTK_WINDOW(window), "Tweetz");
	gtk_window_set_default_size(GTK_WINDOW(window), 1024, 600);
	// make a box with all elements embedded within it
	GtkWidget *box = create_box(sock_act);
	gtk_container_add(GTK_CONTAINER(window), box);
	gtk_widget_show_all(window);
}

static GtkWidget *create_box(GIOChannel *sock_act){
	// This is the main box...
	GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_set_name(box, "main_box");
	// This is the scrolled area
	GtkWidget *sc_win = create_scrolled(sock_act);
	GtkWidget *t_area = gtk_bin_get_child(GTK_BIN(sc_win));
	// This is the sub_box
	GtkWidget *sub_box = create_sub_box(t_area);
	// Embed the scroll area. Expand and fill all available space
	gtk_box_pack_start(GTK_BOX(box), sc_win, TRUE, TRUE, 0);
	// Embed the sub_box, but do not expand and fill available space
	gtk_box_pack_start(GTK_BOX(box), sub_box, FALSE, FALSE, 0);
	return box;
}

static GtkWidget *create_scrolled(GIOChannel *sock_act){
	// This is the scrolled pane...
	GtkWidget *sc_win = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_set_name(sc_win, "scrolled_window");
	gtk_widget_set_margin_top(sc_win, 50);
	// This is the text area...
	GtkWidget *text_area = gtk_text_view_new();	
	gtk_widget_set_name(text_area, "text_area");
	gtk_text_view_set_editable(GTK_TEXT_VIEW(text_area), FALSE);
	gtk_text_view_set_justification(GTK_TEXT_VIEW(text_area),
	GTK_JUSTIFY_LEFT);
	gtk_text_view_set_accepts_tab(GTK_TEXT_VIEW(text_area), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(text_area),
	GTK_WRAP_WORD);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(text_area), FALSE);
	// Creating a GIO channel to monitor the socket on text_area...
	g_io_add_watch(sock_act, G_IO_IN, net_event, text_area);
	// Adding the text area to the scrolled pane...
	gtk_container_add(GTK_CONTAINER(sc_win), text_area);
	return sc_win;
}

static GtkWidget *create_sub_box(GtkWidget *t_area){
	// Creating the struct to pass the t_entry and t_area
	pass_data *entries = malloc(sizeof(pass_data));
	// This is the sub_box...
	GtkWidget *sub_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_name(sub_box, "sub_box");
	// This is the text entry...
	GtkWidget *t_entry = gtk_entry_new();
	gtk_widget_set_name(t_entry, "text_entry");
	gtk_entry_set_max_width_chars(GTK_ENTRY(t_entry), 290);
	gtk_widget_set_margin_start(t_entry, 50);
	gtk_widget_set_margin_end(t_entry, 50);
	gtk_widget_set_margin_top(t_entry, 50);
	gtk_widget_set_margin_bottom(t_entry, 50);
	// This is the send button...
	GtkWidget *s_button = gtk_button_new();
	gtk_widget_set_name(s_button, "send_button");
	GtkWidget *image = gtk_image_new_from_icon_name("mail-send", 
	GTK_ICON_SIZE_BUTTON);
	gtk_button_set_image(GTK_BUTTON(s_button), image);
	gtk_widget_set_margin_top(s_button, 50);
	gtk_widget_set_margin_bottom(s_button, 50);
	gtk_widget_set_margin_end(s_button, 50);
	// emitting a clicked signal upon button press
	gtk_button_clicked(GTK_BUTTON(s_button));
	entries->t_entry = t_entry;
	entries->t_area = t_area;
	// constructing the signal callback function to handle the event
	// also pass in the data for text entries and areas
	g_signal_connect_data(s_button, "clicked", 
	G_CALLBACK(on_s_button_clicked), entries, (GClosureNotify)free, 0);
	// Embedding the text entry and button in the sub_box...
	gtk_box_pack_start(GTK_BOX(sub_box), t_entry, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(sub_box), s_button, FALSE, FALSE, 0);
	return sub_box;
}

void on_s_button_clicked(GtkButton *b, pass_data *entries){
	GtkWidget *window = gtk_widget_get_toplevel(GTK_WIDGET(b));
	/*
	GtkWidget *t_entry = find_child(window, "text_entry");
	GtkWidget *t_area = find_child(window, "text_area");
	*/
	GtkWidget *t_entry = entries->t_entry;
	GtkWidget *t_area = entries->t_area;
	const gchar *text_input = gtk_entry_get_text(GTK_ENTRY(t_entry));
	g_strlcpy(temp_buff, text_input, BUFFER_SIZE);
	g_strlcat(temp_buff, "\n", BUFFER_SIZE);
	gtk_editable_delete_text(GTK_EDITABLE(t_entry), 0, -1);
	gchar *check = event_check();
	if(check != NULL){
		print_buff(GTK_TEXT_VIEW(t_area), check);
		//gtk_text_buffer_set_text(tb, temp_buff, -1);
	}
}

void print_buff(GtkTextView *t_area, const gchar *text_input){
	GtkTextBuffer *tb = gtk_text_view_get_buffer(GTK_TEXT_VIEW(t_area));
	GtkTextMark *tm = gtk_text_buffer_get_insert(tb);
	GtkTextIter iter;
	gtk_text_buffer_get_iter_at_mark(tb, &iter, tm);
	gtk_text_iter_forward_to_end(&iter);
	gtk_text_buffer_insert(tb, &iter, text_input, -1);
}
/*
GtkWidget *find_child(GtkWidget *parent, const gchar *name){
	if(g_ascii_strcasecmp(gtk_widget_get_name(parent), name) == 0)
		return parent;
	if(GTK_IS_BIN(parent)){
		GtkWidget *child = gtk_bin_get_child(GTK_BIN(parent));
		return find_child(child, name);
	}
	if(GTK_IS_CONTAINER(parent)){
		GList *children = gtk_container_get_children(GTK_CONTAINER(parent));
		if(children != NULL){
			do{
					GtkWidget *widget = find_child(children->data, name);
					if(widget != NULL) return widget;
			}while((children = g_list_next(children)) != NULL);
		}
	}
	return NULL;
}
*/
