#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define PAYLOAD_SIZE 256
#define BUFFER_SIZE 1024

// State macros
#define S_OFFLINE 0
#define S_LOGIN_SENT 1
#define S_ONLINE 2
#define S_POST_SENT 3
#define S_RETRIEVE_SENT 4
#define S_LOGOUT_SENT 5
#define S_SUBSCRIBE_SENT 6
#define S_UNSUBSCRIBE_SENT 7
#define S_ERROR 8

// Event macros
#define E_LOGIN 0
#define E_SUBSCRIBE 1
#define E_UNSUBSCRIBE 2
#define E_POST 3
#define E_RETRIEVE 4
#define E_UNRECOGNIZED 5
#define E_RESET 6
#define E_LOGOUT 7
#define E_KILL 8

// Opcode macros
#define O_SESSION_RESET 0x00
#define O_SESSION_RESET_ACK 0x01
#define O_SESSION_KILL 0xD0
#define O_LOGIN_ERROR 0xF0
#define O_LOGIN 0x10
#define O_LOGIN_ACK_SUCCESS 0x80
#define O_LOGIN_ACK_FAILURE 0x81
#define O_SUBSCRIBE 0x20
#define O_SUBSCRIBE_ACK_SUCCESS 0x90
#define O_SUBSCRIBE_ACK_FAILURE 0x91
#define O_UNSUBSCRIBE 0x21
#define O_UNSUBSCRIBE_ACK_SUCCESS 0xA0
#define O_UNSUBSCRIBE_ACK_FAILURE 0xA1
#define O_POST 0x30
#define O_POST_ACK 0xB0
#define O_FORWARD 0xB1
#define O_FORWARD_ACK 0x31
#define O_RETRIEVE 0x40
#define O_RETRIEVE_ACK 0xC0
#define O_RETRIEVE_ACK_END 0xC1
#define O_RETRIEVE_ACK_FAILURE 0xC2
#define O_LOGOUT 0x1F
#define O_LOGOUT_ACK 0x8F

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

int ret, client_socket, server_socket, state, maxfd, charLen, packet_size;
char temp_buff[BUFFER_SIZE], *splitter, *endptr;
struct sockaddr_in servaddr, clientaddr;
unsigned int serv_size, cli_size;
int kill_client;
typedef struct data_format{
	char magic_bytes[2];
	unsigned char opcode;
	unsigned char text_length;
	uint32_t token;
	uint32_t message_ID;
	union payload_type{
		long int retrieve_msgs; //the n in retrieve#n
		char payload[PAYLOAD_SIZE]; //include space for new line
	}pt;
}data_format;
data_format send_packet, recv_packet;
int initialize(){
	kill_client = FALSE;
	packet_size = sizeof(data_format);
	charLen = 0;
	server_socket = socket(AF_INET, SOCK_DGRAM, 0);
	send_packet.magic_bytes[0] = 'M';
	send_packet.magic_bytes[1] = 'B';
	state = S_OFFLINE;
	if(server_socket < 0){
		perror("socket() failed");
		return -1;
	}
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if(client_socket < 0){
		perror("socket() failed");
		return -1;
	}

	serv_size = sizeof(servaddr);
	memset(&servaddr, 0, serv_size);
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(32000);

	cli_size = sizeof(clientaddr);
	memset(&clientaddr, 0, cli_size);
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	clientaddr.sin_port = 0; // let the OS choose a free port

	// binding the client's socket to its address and port
	if(bind(client_socket, (struct sockaddr*) &clientaddr, cli_size) < 0){
		perror("bind() failed");
		return -1;
	}
	return 0;
}

char *event_check(){
	if(strlen(temp_buff) > 1){
		splitter = strtok(temp_buff, "#");
		if(strcmp(splitter, "login") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						send_packet.opcode = O_LOGIN;
						strcpy(send_packet.pt.payload, splitter);
						sendto(client_socket, &send_packet, packet_size,
						0, (struct sockaddr*) &servaddr, serv_size);
						state = S_LOGIN_SENT;
						return NULL; //successful event received
					break;
					case S_ONLINE:
						strcpy(temp_buff, "Error: Invalid event for"
						" the online state\n");
						return temp_buff;
					break;
					case S_ERROR:
						strcpy(temp_buff, "We are in an error state...\n");
						return temp_buff;
					break;
					default:
						strcpy(temp_buff, "Error: waiting for ack from"
						" previous event\n");
						return temp_buff;
					break;
					}
				}
			}
		}
		else if(strcmp(splitter, "subscribe") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						strcpy(temp_buff, "error#must_login_first\n");
						return temp_buff;
					break;
					case S_ONLINE:
						send_packet.opcode = O_SUBSCRIBE;
						strcpy(send_packet.pt.payload, splitter);
						sendto(client_socket, &send_packet, packet_size,
						0, (struct sockaddr*) &servaddr, serv_size);
						state = S_SUBSCRIBE_SENT;
						return NULL; //successful event received
					break;
					case S_ERROR:
						strcpy(temp_buff, "We are in an error state...\n");
						return temp_buff;
					break;
					default:
						strcpy(temp_buff, "Error: waiting for ack from"
						" previous event\n");
						return temp_buff;
					break;
					}
				}
			}
		}
		else if(strcmp(splitter, "unsubscribe") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						strcpy(temp_buff, "error#must_login_first\n");
						return temp_buff;
					break;
					case S_ONLINE:
						send_packet.opcode = O_UNSUBSCRIBE;
						strcpy(send_packet.pt.payload, splitter);
						sendto(client_socket, &send_packet, packet_size,
						0, (struct sockaddr*) &servaddr, serv_size);
						state = S_UNSUBSCRIBE_SENT;
						return NULL; //successful event received
					break;
					case S_ERROR:
						strcpy(temp_buff, "We are in an error state...\n");
						return temp_buff;
					break;
					default:
						strcpy(temp_buff, "Error: waiting for ack from"
						" previous event\n");
						return temp_buff;
					break;
					}
				}
			}
		}
		else if(strcmp(splitter, "post") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						strcpy(temp_buff, "error#must_login_first\n");
						return temp_buff;
					break;
					case S_ONLINE:
						send_packet.opcode = O_POST;
						strcpy(send_packet.pt.payload, splitter);
						sendto(client_socket, &send_packet, packet_size,
						0, (struct sockaddr*) &servaddr, serv_size);
						state = S_POST_SENT;
						return NULL; //successful event received
					break;
					case S_ERROR:
						strcpy(temp_buff, "We are in an error state...\n");
						return temp_buff;
					break;
					default:
						strcpy(temp_buff, "Error: waiting for ack from"
						" previous event\n");
						return temp_buff;
					break;
					}
				}
			}
		}
		else if(strcmp(splitter, "retrieve") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						strcpy(temp_buff, "error#must_login_first\n");
						return temp_buff;
					break;
					case S_ONLINE:
						send_packet.opcode = O_RETRIEVE;
						splitter = strtok(splitter, "\n");
						send_packet.pt.retrieve_msgs = 
						strtol(splitter, &endptr, 10);
						if(send_packet.pt.retrieve_msgs > 0 && 
						*endptr == '\0' && errno != ERANGE){
							sendto(client_socket, &send_packet, packet_size,
							0, (struct sockaddr*) &servaddr, serv_size);
							state = S_RETRIEVE_SENT;
							return NULL; //successful event received
						}
						else{
							errno = 0;
							strcpy(temp_buff, "Error: Please enter a"
							" valid number\n");
							return temp_buff;
						}
					break;
					case S_ERROR:
						strcpy(temp_buff, "We are in an error state...\n");
						return temp_buff;
					break;
					default:
						strcpy(temp_buff, "Error: waiting for ack from"
						" previous event\n");
						return temp_buff;
					break;
					}
				}
			}
		}
		else if(strcmp(splitter, "logout") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						strcpy(temp_buff, "error#must_login_first\n");
						return temp_buff;
					break;
					case S_ONLINE:
						send_packet.opcode = O_LOGOUT;
						sendto(client_socket, &send_packet, packet_size,
						0, (struct sockaddr*) &servaddr, serv_size);
						state = S_LOGOUT_SENT;
						return NULL; //successful event received
					break;
					case S_ERROR:
						strcpy(temp_buff, "We are in an error state...\n");
						return temp_buff;
					break;
					default:
						strcpy(temp_buff, "Error: waiting for ack from"
						" previous event\n");
						return temp_buff;
					break;
					}
				}
			}
		}
		else if(strcmp(splitter, "reset") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					strcpy(temp_buff, "Resetting the state...\n");
					send_packet.opcode = O_SESSION_RESET;
					sendto(client_socket, &send_packet, packet_size,
					0, (struct sockaddr*) &servaddr, serv_size);
					state = S_OFFLINE;
					return temp_buff;
				}
			}
		}
		else if(strcmp(splitter, "kill") == 0){
			splitter = strtok(NULL, "\0");
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					strcpy(temp_buff, "Killing this client and server\n");
					send_packet.opcode = O_SESSION_KILL;
					sendto(client_socket, &send_packet, packet_size,
					0, (struct sockaddr*) &servaddr, serv_size);
					state = S_OFFLINE;
					return temp_buff;
				}
			}
		}
	}
	strcpy(temp_buff, "Error: Unrecognized command format\n");
	return temp_buff;
}

char *net_check(){
	recvfrom(client_socket, &recv_packet, packet_size, 
	0, (struct sockaddr *) &servaddr, &serv_size);
	if(recv_packet.magic_bytes[0] == 'M' && 
	recv_packet.magic_bytes[1] == 'B'){ 
		switch(state){
		case S_ONLINE:
			switch(recv_packet.opcode){
			case O_FORWARD:
				strcpy(temp_buff, recv_packet.pt.payload);
				strcat(temp_buff, "\n");
				//printf("%s\n", recv_packet.pt.payload);
				send_packet.opcode = O_FORWARD_ACK;
				sendto(client_socket, &send_packet, packet_size, 
				0, (struct sockaddr *) &servaddr, serv_size);
				return temp_buff;
			break;
			case O_LOGIN_ERROR:
				strcpy(temp_buff, "error#must_login_first\n");
				//printf("error#must_login_first\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;
			}
		break;
		case S_LOGIN_SENT:
			switch(recv_packet.opcode){
			case O_LOGIN_ACK_SUCCESS:
				//strcpy(temp_buff, "login_ack#successful\n");
				snprintf(temp_buff, BUFFER_SIZE,
				"login_ack#successful\nThis is the token: %u\n",
				recv_packet.token);
				//printf("login_ack#successful\n");
				//printf("This is the token: %u\n", recv_packet.token);
				send_packet.token = recv_packet.token;
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_LOGIN_ACK_FAILURE:
				strcpy(temp_buff, "login_ack#failed\n");
				//printf("login_ack#failed\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;
			}
		break;
		case S_POST_SENT:
			switch(recv_packet.opcode){
			case O_POST_ACK:
				strcpy(temp_buff, "post_ack#successful\n");
				//printf("post_ack#successful\n");
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_LOGIN_ERROR:
				strcpy(temp_buff, "error#must_login_first\n");
				//printf("error#must_login_first\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;
			}
		break;
		case S_RETRIEVE_SENT:
			switch(recv_packet.opcode){
			case O_RETRIEVE_ACK:
				strcpy(temp_buff, recv_packet.pt.payload);
				strcat(temp_buff, "\n");
				return temp_buff;
				//printf("%s\n", (recv_packet.pt.payload));
			break;
			case O_RETRIEVE_ACK_END:
				state = S_ONLINE;
			break;
			case O_RETRIEVE_ACK_FAILURE:
				strcpy(temp_buff, "The retrieve number is larger "
				"than the amount present\n");
				//printf("The retrieve number is larger than the amount"
				//" present\n");
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_LOGIN_ERROR:
				strcpy(temp_buff, "error#must_login_first\n");
				//printf("error#must_login_first\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;	
			}
		break;
		case S_LOGOUT_SENT:
			switch(recv_packet.opcode){
			case O_LOGOUT_ACK:
				strcpy(temp_buff, "logout_ack#successful\n");
				//printf("logout_ack#successful\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_LOGIN_ERROR:
				strcpy(temp_buff, "error#must_login_first\n");
				//printf("error#must_login_first\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;
			}
		break;
		case S_SUBSCRIBE_SENT:
			switch(recv_packet.opcode){
			case O_SUBSCRIBE_ACK_SUCCESS:
				strcpy(temp_buff, "subscribe_ack#successful\n");
				//printf("subscribe_ack#successful\n");
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_SUBSCRIBE_ACK_FAILURE:
				strcpy(temp_buff, "subscribe_ack#failed\n");
				//printf("subscribe_ack#failed\n");
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_LOGIN_ERROR:
				strcpy(temp_buff, "error#must_login_first\n");
				//printf("error#must_login_first\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;
			}
		break;
		case S_UNSUBSCRIBE_SENT:
			switch(recv_packet.opcode){
			case O_UNSUBSCRIBE_ACK_SUCCESS:
				strcpy(temp_buff, "unsubscribe_ack#successful\n");
				//printf("unsubscribe_ack#successful\n");
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_UNSUBSCRIBE_ACK_FAILURE:
				strcpy(temp_buff, "unsubscribe_ack#failed\n");
				//printf("unsubscribe_ack#failed\n");
				state = S_ONLINE;
				return temp_buff;
			break;
			case O_LOGIN_ERROR:
				strcpy(temp_buff, "error#must_login_first\n");
				//printf("error#must_login_first\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			default: state = S_ERROR; break;
			}
		break;
		case S_ERROR:
			//printf("We are in an error state. Something went wrong\n");
			switch(recv_packet.opcode){
			case O_SESSION_RESET_ACK:
				strcpy(temp_buff, "Session has been reset...\n");
				//printf("Session has been reset...\n");
				state = S_OFFLINE;
				return temp_buff;
			break;
			}
		break;
		default: state = S_OFFLINE; break;
		}
	}
	return NULL;
}
