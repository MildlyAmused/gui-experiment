#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <pthread.h>
#include <list>
#include <vector>
#include <iterator>
#include <map>
#define PAYLOAD_SIZE 256
#define BUFFER_SIZE 256

// max time allowed before timeout kicks in
#define TIMEOUT 100 

// State macros
#define S_OFFLINE 0
#define S_ONLINE 1
#define S_MESSAGE_FORWARD 2
#define S_ERROR 3

// Opcode macros
#define O_SESSION_RESET 0x00
#define O_SESSION_RESET_ACK 0x01
#define O_SESSION_KILL 0xD0
#define O_LOGIN_ERROR 0xF0
#define O_LOGIN 0x10
#define O_LOGIN_ACK_SUCCESS 0x80
#define O_LOGIN_ACK_FAILURE 0x81
#define O_SUBSCRIBE 0x20
#define O_SUBSCRIBE_ACK_SUCCESS 0x90
#define O_SUBSCRIBE_ACK_FAILURE 0x91
#define O_UNSUBSCRIBE 0x21
#define O_UNSUBSCRIBE_ACK_SUCCESS 0xA0
#define O_UNSUBSCRIBE_ACK_FAILURE 0xA1
#define O_POST 0x30
#define O_POST_ACK 0xB0
#define O_FORWARD 0xB1
#define O_FORWARD_ACK 0x31
#define O_RETRIEVE 0x40
#define O_RETRIEVE_ACK 0xC0
#define O_RETRIEVE_ACK_END 0xC1
#define O_RETRIEVE_ACK_FAILURE 0xC2
#define O_LOGOUT 0x1F
#define O_LOGOUT_ACK 0x8F

using namespace std;

typedef struct data_format{
	char magic_bytes[2];
	unsigned char opcode;
	unsigned char text_length;
	uint32_t token;
	uint32_t message_ID;
	union payload_type{
		long int retrieve_msgs; //the n in retrieve#n
		char payload[PAYLOAD_SIZE]; //include space for new line
	}pt;
}data_format;

typedef struct user_profile{
	char username[PAYLOAD_SIZE];
	char password[PAYLOAD_SIZE];
	struct sockaddr_in6 client_addr;
	uint32_t token;
	int state;
	int post_count;
	time_t last_time_accessed;
	list<user_profile*> subscribed_to;
	// list of users this user is subscribed to
	list<user_profile*> subscribed_from;
	// list of users this user has been subscribed by
}user_profile;

typedef struct post_type{
	string username;
	string post;
	post_type(string i_username, string i_post)
	: username(move(i_username)), post(move(i_post)){}
}post_type;

	int ret, recv_len, server_socket;
	struct sockaddr_in6 servaddr, cliaddr;
	struct ifaddrs *if_list = NULL;
	struct ifaddrs *if_it = NULL;
	char buffer[BUFFER_SIZE];
	pthread_mutex_t lock;
	bool kill_serv = false;
	socklen_t ser_len, cli_len;
	data_format send_packet, recv_packet;
	map<string, user_profile*> all_users;
	map<string, user_profile*>::iterator it;
	map<uint32_t, user_profile*> online_users;
	map<uint32_t, user_profile*>::iterator itr;
	vector<post_type> p_list;
	vector<post_type>::reverse_iterator p_it;

// will free all alocated memory 
void destroy(){
	for(auto& i: all_users){
		i.second->subscribed_to.clear();
		i.second->subscribed_from.clear();
		delete i.second;
	}
	p_list.clear();
	all_users.clear();
	online_users.clear();
}

// creates user profiles for all users and 
// maps their usernames to their profiles
int initialize(){
	char *temp_ptr;
	FILE *fp_1;
	user_profile *temp;
	fp_1 = fopen("users.txt", "r");
	if(fp_1 == NULL){
		perror("Error encountered opening users");
		return -1;
	}
	if(fgets(buffer, BUFFER_SIZE, fp_1) != NULL){
		while(!feof(fp_1)){
			temp = new user_profile;
			if(temp != NULL){
				temp_ptr = strtok(buffer, "&");
				strcpy(temp->username, temp_ptr);
				temp_ptr = strtok(NULL, "\n");
				strcpy(temp->password, temp_ptr);
				temp->state = S_OFFLINE;
				temp->token = 0;
				temp->last_time_accessed = time(NULL);
				temp->post_count = 0;
				all_users.insert(pair<string, 
				user_profile*>(temp->username, temp));
			}
			else{
				fclose(fp_1);
				destroy();
				return -1;
			}
			fgets(buffer, BUFFER_SIZE, fp_1);
		}
		fclose(fp_1);
		return 0;
	}
	else return -1;
}

// gets called at a login prompt and returns a token 
// to the corresponding user otherwise 0 if it fails
uint32_t authenticate(char *username, char *password){
	uint32_t token;
	it = all_users.find(username);
	if(it != all_users.end() && it->second->state == S_OFFLINE){ 
	// check to see if the user exists and that it is not online
		if(strcmp(it->second->password, password) == 0){ 
		// check to see if passwords match
			do{
				srand(time(NULL)); // initialize current time as seed
				token = rand(); // generate a random token
				itr = online_users.find(token);
				// check if other users have the same token
			}while(itr != online_users.end() 
			&& token != 0);
			online_users.insert(itr, pair
			<uint32_t, user_profile*>(token, it->second));
			// add the online user
			it->second->token = token;
			it->second->state = S_ONLINE;
			it->second->last_time_accessed = time(NULL);
			memcpy(&(it->second->client_addr), 
			&cliaddr, sizeof(struct sockaddr_in6));
			return token;
		}
	}
	return 0;
}

// gets called at logout or until timeout
// returns true if can logout otherwise false
bool deauthenticate(uint32_t token){
	itr = online_users.find(token);
	if(itr != online_users.end()){
	// check to see if the user is online
		itr->second->token = 0;
		itr->second->state = S_OFFLINE;
		// take the user off the online 
		// list
		online_users.erase(itr);
		return true;
	}
	return false;
}

// check whether a client has entered the timeout phase or not
void timeout(time_t& curr_time){
	time_t diff;
	user_profile *r_user;
	for(auto& i: online_users){
		r_user = i.second;
		diff = curr_time - r_user->last_time_accessed;
		if(diff > TIMEOUT)
			deauthenticate(r_user->token);
	}
}

// implementing subscribe...
int subscribe(char *target_user, uint32_t token){
	user_profile *t_user;
	user_profile *r_user;
	itr = online_users.find(token);
	if(itr != online_users.end()){
	// check to see if the requesting user is online
		r_user = itr->second;
		it = all_users.find(target_user);
		r_user->last_time_accessed = time(NULL);
		// find the target user
		if(it != all_users.end()){
			t_user = it->second;
			t_user->subscribed_from.push_back(r_user);
			// add the requesting user to the target user's subscribe list
			r_user->subscribed_to.push_back(t_user);
			// add the target user to the requesting user's subscribe list
			return 1; // success
		}
		return 0; // target user is not found
	}
	return -1; // requesting user is not logged in
}

// implementing unsubscribe...
int unsubscribe(char *target_user, uint32_t token){
	unsigned int size;
	user_profile *t_user;
	user_profile *r_user;
	itr = online_users.find(token);
	if(itr != online_users.end()){
	// check to see if the requesting user is online
		r_user = itr->second;
		it = all_users.find(target_user);
		r_user->last_time_accessed = time(NULL);
		// find the target user
		if(it != all_users.end()){
			t_user = it->second;
			size = t_user->subscribed_from.size();
			t_user->subscribed_from.remove(r_user);
			if(size != t_user->subscribed_from.size()){
				r_user->subscribed_to.remove(t_user);
				return 1; // success
			}
		}
		return 0; //target user is not found
	}
	return -1; // requesting user is not logged in
}

// retrieve n posts...
bool retrieve(long int count, uint32_t token){ 
	long int total_posts = 0;
	itr = online_users.find(token);
	user_profile *r_user;
	if(itr != online_users.end()){
		r_user = itr->second;
		r_user->last_time_accessed = time(NULL);
		// find the user profile...
		for(auto& j: r_user->subscribed_to){
			total_posts += j->post_count;
		}
		if(count <= total_posts){
			for(p_it = p_list.rbegin(); p_it != p_list.rend(); ++p_it)
				for(auto& j: r_user->subscribed_to)
					if(j->username == p_it->username && count != 0){
						strcpy(send_packet.pt.payload, p_it->post.c_str());
						send_packet.opcode = O_RETRIEVE_ACK;
						send_packet.text_length = p_it->username.size();
						sendto(server_socket, &send_packet,
						sizeof(send_packet), 0, 
						(struct sockaddr*) &(r_user->client_addr), 
						sizeof(r_user->client_addr));
						count--;
					}
			send_packet.opcode = O_RETRIEVE_ACK_END;
			sendto(server_socket, &send_packet, sizeof(send_packet), 0, 
			(struct sockaddr*) &(r_user->client_addr), 
			sizeof(r_user->client_addr));
			return true;
		}
		send_packet.opcode = O_RETRIEVE_ACK_FAILURE;
		sendto(server_socket, &send_packet, sizeof(send_packet), 0, 
		(struct sockaddr*) &(r_user->client_addr), 
		sizeof(r_user->client_addr));
		return true;
	}
	return false;
}

// executed after a post. Forwards post to all subscribed users
void forward(char *post, user_profile *r_user){
	for(auto& i: r_user->subscribed_from){
	// send the post to all users subscribed to this one
		if(i->state == S_ONLINE){
		// check to make sure this user is online before forwarding
			i->state = S_MESSAGE_FORWARD;
			send_packet.opcode = O_FORWARD;
			send_packet.text_length = strlen(post);
			strcpy(send_packet.pt.payload, post);
			sendto(server_socket, &send_packet, sizeof(send_packet),
			0, (struct sockaddr*) &(i->client_addr), 
			sizeof(i->client_addr));
		}
	}
}

// post command received. Store the post in a vector and forward
// to all users subscribed to the sender
bool post(char *post, uint32_t token){
	itr = online_users.find(token);
	user_profile *r_user;
	if(itr != online_users.end()){
		r_user = itr->second;
		p_list.emplace_back(r_user->username, post);
		++r_user->post_count;
		// increment the post count
		r_user->last_time_accessed = time(NULL);
		// update the time accessed variable
		forward(post, r_user);
		return true;
	}
	return false;
}

// forward acknowledgement received. Check if user is valid.
// if they are, change their state back to online.
bool forward(uint32_t token){
	user_profile *r_user;
	itr = online_users.find(token);
	if(itr != online_users.end()){
		r_user = itr->second;
		if(r_user->state == S_MESSAGE_FORWARD){
			r_user->state = S_ONLINE;
			return true;
		}
	}
	return false;
}

// generating a reset message to all other users connected to the server
void send_to_all(unsigned char opcode){
	user_profile *r_user;
	send_packet.opcode = opcode;
	for(auto& i: online_users){
		r_user = i.second;
		sendto(server_socket, &send_packet, sizeof(send_packet), 0,
		(struct sockaddr*) &(r_user->client_addr), 
		sizeof(r_user->client_addr));
	}
}

// find the correct iterface
bool find_ip(){
	getifaddrs(&if_list);
	unsigned int flags;
	struct sockaddr_in6 *sa;
	for(if_it = if_list; if_it != NULL; if_it = if_it->ifa_next){
		flags = if_it->ifa_flags;
		if(!(flags & IFF_LOOPBACK) && 
		(flags & IFF_UP) && (flags & IFF_RUNNING)){
			if(if_it->ifa_addr != NULL && 
			if_it->ifa_addr->sa_family == AF_INET6){
				sa = (struct sockaddr_in6*) if_it->ifa_addr;
				if(!IN6_IS_ADDR_LINKLOCAL(&(sa->sin6_addr))){
					//inet_ntop(AF_INET6, &(sa->sin6_addr.s6_addr), 
					//buffer, BUFFER_SIZE);
					//printf("%s\n", buffer);
					memcpy(&servaddr, sa, sizeof(struct sockaddr_in6));
					return true;
				}
			}
		}
	}
	if (if_list != NULL) freeifaddrs(if_list);
	return false;
}

void *timer_thread(void *arg){
	time_t curr_time;
	//printf("Started timer thread\n");
	pthread_mutex_lock(&lock);
	while(kill_serv == false){
		// call timeout here...
		//printf("Timer thread reports kill_serv: 
		//%d\n", kill_serv);
		curr_time = time(NULL);
		timeout(curr_time);
		pthread_mutex_unlock(&lock);
		sleep(1);
		pthread_mutex_lock(&lock);
	}
	pthread_mutex_unlock(&lock);
	//printf("Finished timer thread\n");
	return NULL;
}

void *server_thread(void *arg){
	cli_len = sizeof(cliaddr);
	ser_len = sizeof(servaddr);
	char *username, *password;
	char *p_recv;
	long int count;
	uint32_t token;
	int temp_val;
	send_packet.magic_bytes[0] = 'M';
	send_packet.magic_bytes[1] = 'B';
	const int packet_size = sizeof(data_format);
	int init = initialize();
	if(init == -1) return (void*)1;
	server_socket = socket(AF_INET6, SOCK_DGRAM, 0);
	if(server_socket < 0){
		perror("socket() failed");
		return (void*)1;
	}
	if(!find_ip()) return (void*)1;
	servaddr.sin6_port = htons(32000);
	bind(server_socket, (struct sockaddr*) &servaddr, ser_len);
	if(pthread_mutex_init(&lock, NULL) != 0){
		perror("Mutex init failed");
		return (void*)1;
	}
	pthread_t tid;
	pthread_create(&tid, NULL, timer_thread, NULL);
	pthread_mutex_lock(&lock);
	while(kill_serv == false){
		pthread_mutex_unlock(&lock);
		recv_len = recvfrom(server_socket, &recv_packet, 
		packet_size, 0, (struct sockaddr*) &cliaddr, &cli_len);
		pthread_mutex_lock(&lock);
		if(recv_len > 0){
			if(recv_packet.magic_bytes[0] == 'M' && 
			recv_packet.magic_bytes[1] == 'B'){
				switch(recv_packet.opcode){
				case O_LOGIN:
					username = strtok(recv_packet.pt.payload, "&");
					password = strtok(NULL, "\n");
					if(password != NULL){
						token = authenticate(username, password);
						if(token != 0){
							send_packet.opcode = O_LOGIN_ACK_SUCCESS;
							send_packet.token = token;
						}
						else send_packet.opcode = O_LOGIN_ACK_FAILURE;
						do{
							ret = sendto(server_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr*) &cliaddr, cli_len);
						}while(ret <= 0);
					}
					else{
						send_packet.opcode = O_LOGIN_ACK_FAILURE;
						do{
							ret = sendto(server_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr*) &cliaddr, cli_len);
						}while(ret <= 0);
					}
				break;
				case O_LOGOUT:
					token = recv_packet.token;
					if(deauthenticate(token))
						send_packet.opcode = O_LOGOUT_ACK;
					else
						send_packet.opcode = O_LOGIN_ERROR;
					do{
						ret = sendto(server_socket, &send_packet, 
						packet_size, 0, (struct sockaddr*) &cliaddr, 
						cli_len);
					}while(ret <= 0);
				break;
				case O_POST:
					token = recv_packet.token;
					p_recv = recv_packet.pt.payload;
					p_recv = strtok(p_recv, "\n");
					if(post(p_recv, token))
						send_packet.opcode = O_POST_ACK;
					else
						send_packet.opcode = O_LOGIN_ERROR;
					do{
						ret = sendto(server_socket, &send_packet, 
						packet_size, 0, (struct sockaddr*) &cliaddr, 
						cli_len);
					}while(ret <= 0);
				break;
				case O_RETRIEVE:
					token = recv_packet.token;
					count = recv_packet.pt.retrieve_msgs;
					if(retrieve(count, token) != true){
						send_packet.opcode = O_LOGIN_ERROR;
						do{
							ret = sendto(server_socket, &send_packet, 
							packet_size, 0, (struct sockaddr*) &cliaddr, 
							cli_len);
						}while(ret <= 0);
					}
				break;
				case O_SUBSCRIBE:
					token = recv_packet.token;
					username = recv_packet.pt.payload;
					username = strtok(username, "\n");
					temp_val = subscribe(username, token);
					if(temp_val == 1)
						send_packet.opcode = O_SUBSCRIBE_ACK_SUCCESS;
					else if(temp_val == 0)
						send_packet.opcode = O_SUBSCRIBE_ACK_FAILURE;
					else
						send_packet.opcode = O_LOGIN_ERROR;
					do{
						ret = sendto(server_socket, &send_packet, 
						packet_size, 0, (struct sockaddr*) &cliaddr, 
						cli_len);
					}while(ret <= 0);
				break;
				case O_UNSUBSCRIBE:
					token = recv_packet.token;
					username = recv_packet.pt.payload;
					username = strtok(username, "\n");
					temp_val = unsubscribe(username, token); 
					if(temp_val == 1)
						send_packet.opcode = O_UNSUBSCRIBE_ACK_SUCCESS;
					else if(temp_val == 0)
						send_packet.opcode = O_UNSUBSCRIBE_ACK_FAILURE;
					else
						send_packet.opcode = O_LOGIN_ERROR;
					do{
						ret = sendto(server_socket, &send_packet, 
						packet_size, 0, (struct sockaddr*) &cliaddr, 
						cli_len);
					}while(ret <= 0);
				break;
				case O_FORWARD_ACK:
					token = recv_packet.token;
					if(forward(token) != true){
						send_packet.opcode = O_LOGIN_ERROR;
						do{
							ret = sendto(server_socket, &send_packet, 
							packet_size, 0, (struct sockaddr*) &cliaddr, 
							cli_len);
						}while(ret <= 0);
					}
				break;
				case O_SESSION_RESET:
					token = recv_packet.token;
					if(deauthenticate(token)){
						send_to_all(O_SESSION_RESET_ACK);
						destroy();
						initialize();
						printf("Server was reset...\n");
					}
				break;
				case O_SESSION_KILL:
					destroy();
					printf("Server was killed...\n");
					kill_serv = true;
				break;
				default:
					printf("Some error occured...\n");
					destroy();
					break;
				break;
				}
			}
		}
		else if(errno != EAGAIN){
			perror("recvfrom() failed");
			destroy();
			return (void*)1;
		}
	}
	pthread_mutex_unlock(&lock);
	pthread_join(tid, NULL);
	pthread_mutex_destroy(&lock);
	return NULL;
}

int main(){
	pthread_t tid;
	pthread_create(&tid, NULL, server_thread, NULL);
	pthread_join(tid, NULL);
	return 0;
}

