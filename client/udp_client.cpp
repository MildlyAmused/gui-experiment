#include <stdio.h>
#include <sys/select.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#define PAYLOAD_SIZE 256
#define BUFFER_SIZE 1024

// State macros
#define S_OFFLINE 0
#define S_LOGIN_SENT 1
#define S_ONLINE 2
#define S_POST_SENT 3
#define S_RETRIEVE_SENT 4
#define S_LOGOUT_SENT 5
#define S_SUBSCRIBE_SENT 6
#define S_UNSUBSCRIBE_SENT 7
#define S_ERROR 8

// Event macros
#define E_LOGIN 0
#define E_SUBSCRIBE 1
#define E_UNSUBSCRIBE 2
#define E_POST 3
#define E_RETRIEVE 4
#define E_UNRECOGNIZED 5
#define E_RESET 6
#define E_LOGOUT 7
#define E_KILL 8

// Opcode macros
#define O_SESSION_RESET 0x00
#define O_SESSION_RESET_ACK 0x01
#define O_SESSION_KILL 0xD0
#define O_LOGIN_ERROR 0xF0
#define O_LOGIN 0x10
#define O_LOGIN_ACK_SUCCESS 0x80
#define O_LOGIN_ACK_FAILURE 0x81
#define O_SUBSCRIBE 0x20
#define O_SUBSCRIBE_ACK_SUCCESS 0x90
#define O_SUBSCRIBE_ACK_FAILURE 0x91
#define O_UNSUBSCRIBE 0x21
#define O_UNSUBSCRIBE_ACK_SUCCESS 0xA0
#define O_UNSUBSCRIBE_ACK_FAILURE 0xA1
#define O_POST 0x30
#define O_POST_ACK 0xB0
#define O_FORWARD 0xB1
#define O_FORWARD_ACK 0x31
#define O_RETRIEVE 0x40
#define O_RETRIEVE_ACK 0xC0
#define O_RETRIEVE_ACK_END 0xC1
#define O_RETRIEVE_ACK_FAILURE 0xC2
#define O_LOGOUT 0x1F
#define O_LOGOUT_ACK 0x8F

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

int ret, client_socket, server_socket, state, maxfd, charLen, packet_size;
char temp_buff[BUFFER_SIZE], *splitter, *endptr;
struct sockaddr_in6 servaddr, clientaddr;
unsigned int serv_size, cli_size;
int kill_client, event;
typedef struct data_format{
	char magic_bytes[2];
	unsigned char opcode;
	unsigned char text_length;
	uint32_t token;
	uint32_t message_ID;
	union payload_type{
		long int retrieve_msgs; //the n in retrieve#n
		char payload[PAYLOAD_SIZE]; //include space for new line
	}pt;
}data_format;
data_format send_packet, recv_packet;
struct ifaddrs *if_list = NULL;
struct ifaddrs *if_it = NULL;

const char get_ll_ip(struct sockaddr_in6* clientaddr){
	getifaddrs(&if_list);
	unsigned int flags;
	struct sockaddr_in6 *sa;
	for(if_it = if_list; if_it != NULL; if_it = if_it->ifa_next){
		flags = if_it->ifa_flags;
		if(!(flags & IFF_LOOPBACK) && (flags & IFF_UP) 
		&& (flags & IFF_RUNNING))
			if(if_it->ifa_addr != NULL && 
			if_it->ifa_addr->sa_family == AF_INET6){
				sa = (struct sockaddr_in6*) if_it->ifa_addr;
				if(!IN6_IS_ADDR_LINKLOCAL(&(sa->sin6_addr))){
					//const char *ret = 
					//inet_ntop(AF_INET6, &(sa->sin6_addr.s6_addr),
					//temp_buff, BUFFER_SIZE);
					//printf("%s\n", temp_buff);
					memcpy(clientaddr, sa, sizeof(struct sockaddr_in6));
					return TRUE;
					//if (ret != NULL) break;
				}
			}
	}
	if (if_list != NULL) freeifaddrs(if_list);
	return FALSE;
}

int main(){
	/*
	int ret;
	int client_socket = 0, server_socket = 0;
	struct sockaddr_in servaddr, clientaddr;
	char buffer[1024];
	int maxfd;
	fd_set read_set;
	FD_ZERO(&read_set);
	unsigned int serv_size = 0, cli_size = 0;

	bool kill = false;
	char *splitter, *endptr;
	int charLen = 0;
	typedef struct data_format{
		char magic_bytes[2];
		unsigned char opcode;
		unsigned char text_length;
		uint32_t token;
		uint32_t message_ID;
		union payload_type{
			long int retrieve_msgs; //the n in retrieve#n
			char payload[PAYLOAD_SIZE]; //include space for new line
		}pt;
	}data_format;
	data_format send_packet, recv_packet;
	send_packet.magic_bytes[0] = 'M';
	send_packet.magic_bytes[1] = 'B';
	const int packet_size = sizeof(data_format);
	int state = S_OFFLINE;
	int event;
	//int token;
	*/

	fd_set read_set;
	FD_ZERO(&read_set);
	kill_client = FALSE;
	packet_size = sizeof(data_format);
	charLen = 0;
	send_packet.magic_bytes[0] = 'M';
	send_packet.magic_bytes[1] = 'B';
	state = S_OFFLINE;
	server_socket = socket(AF_INET6, SOCK_DGRAM, 0);
	if(server_socket < 0){
		perror("socket() failed");
		return -1;
	}
	client_socket = socket(AF_INET6, SOCK_DGRAM, 0);
	if(client_socket < 0){
		perror("socket() failed");
		return -1;
	}

	memset(&servaddr, 0, serv_size);
	servaddr.sin6_family = AF_INET6;
	if(inet_pton(AF_INET6, 
	//"2600:8800:5c00:c71:1d82:ff96:9f97:f211", 
	"2600:8800:c00:945:78b9:298f:1357:dff",
	//"2600:8800:c00:9c4:4045:d7b4:51e0:4c85",
	//"fe80::4045:d7b4:51e0:4c85",
	&(servaddr.sin6_addr.s6_addr)) < 1){
		perror("Could not convert IPv6 text");
		return -1;
	}
	servaddr.sin6_port = htons(32000);
	serv_size = sizeof(servaddr);

	memset(&clientaddr, 0, cli_size);
	const char temp = get_ll_ip(&clientaddr);
	if(temp == FALSE) {
		printf("No local link ipv6 found\n");
		return -1;
	}
	clientaddr.sin6_port = 0; // let the OS choose a free port
	cli_size = sizeof(clientaddr);

	// binding the client's socket to its address and port
	if(bind(client_socket, (struct sockaddr*) &clientaddr, cli_size) < 0){
		perror("bind() failed");
		return -1;
	}
	maxfd = client_socket + 1; 
	while(kill_client == false){
	// Use select to wait on keyboard input or socket receiving
	FD_SET(fileno(stdin), &read_set);
	FD_SET(client_socket, &read_set);
	select(maxfd, &read_set, NULL, NULL, NULL);
	if(FD_ISSET(fileno(stdin), &read_set)){
		fgets(temp_buff, sizeof(temp_buff), stdin);
		if(strlen(temp_buff) > 1){
			splitter = strtok(temp_buff, "#"); 
			// if there is no #, strtok will stop at the null 
			// terminator and return the whole string.
			if (strcmp(splitter, "login") == 0) event = E_LOGIN;
				else if(strcmp(splitter, "subscribe") == 0) 
					event = E_SUBSCRIBE;
				else if(strcmp(splitter, "unsubscribe") == 0) 
					event = E_UNSUBSCRIBE;
				else if(strcmp(splitter, "post") == 0) 
					event = E_POST;
				else if(strcmp(splitter, "retrieve") == 0) 
					event = E_RETRIEVE;
				else if(strcmp(splitter, "reset") == 0)
					event = E_RESET;
				else if(strcmp(splitter, "logout") == 0)
					event = E_LOGOUT;
				else if(strcmp(splitter, "kill_client") == 0)
					event = E_KILL;
			else event = E_UNRECOGNIZED;
			splitter = strtok(NULL, "\0");
			// This will return null if the previous strtok call
			// did not find a hash.
			if(splitter != NULL){
				charLen = strlen(splitter);
				send_packet.text_length = charLen;
				if(charLen <= 255){
					switch(state){
					case S_OFFLINE:
						switch(event){
						case E_LOGIN:
							send_packet.opcode = O_LOGIN;
							strcpy(send_packet.pt.payload, splitter);
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							state = S_LOGIN_SENT;
						break;
						case E_RESET:
							printf("Resetting the state...\n");
							send_packet.opcode = O_SESSION_RESET;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
						break;
						case E_KILL:
							printf("killing this client and server...\n");
							send_packet.opcode = O_SESSION_KILL;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							kill_client = true;
						break;
						case E_UNRECOGNIZED:
							printf("Error: Unrecognized "
							"command format\n");
						break;
						default:
							printf("error#must_login_first\n");
						break;
						}
					break;
					case S_ONLINE:
						switch(event){
						case E_POST:
							send_packet.opcode = O_POST;
							strcpy(send_packet.pt.payload, splitter);
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							state = S_POST_SENT;
						break;
						case E_RETRIEVE:
							send_packet.opcode = O_RETRIEVE;
							splitter = strtok(splitter, "\n");
							send_packet.pt.retrieve_msgs = strtol(
							splitter, &endptr, 10);
							// convert to base 10 long and perform
							// error checking
							if(send_packet.pt.retrieve_msgs > 0 && 
							*endptr == '\0' && errno != ERANGE){
								// converted number contains no invalid 
								// chars and is within the range of the
								// long
								ret = sendto(client_socket, &send_packet, 
								packet_size, 0, 
								(struct sockaddr *) &servaddr, serv_size);
								if(ret <= 0){
									perror("sendto() failed");
									return -1;
								}
								state = S_RETRIEVE_SENT;
							}
							else{
								errno = 0;
								printf("Error: Please enter a"
								" valid number\n");
							}
						break;
						case E_SUBSCRIBE:
							send_packet.opcode = O_SUBSCRIBE;
							strcpy(send_packet.pt.payload, splitter);
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							state = S_SUBSCRIBE_SENT;
						break;
						case E_UNSUBSCRIBE:
							send_packet.opcode = O_UNSUBSCRIBE;
							strcpy(send_packet.pt.payload, splitter);
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							state = S_UNSUBSCRIBE_SENT;
						break;
						case E_RESET:
							printf("Resetting the state...\n");
							state = S_OFFLINE;
							send_packet.opcode = O_SESSION_RESET;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
						break;
						case E_LOGOUT:
							state = S_LOGOUT_SENT;
							send_packet.opcode = O_LOGOUT;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
						break;
						case E_KILL:
							printf("killing this client and server...\n");
							send_packet.opcode = O_SESSION_KILL;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							kill_client = true;
						break;
						case E_UNRECOGNIZED:
							printf("Error: Unrecognized "
							"command format\n");
						break;
						default:
							printf("Error: Invalid event for"
							" the online state\n");
						break;
						}
					break;
					case S_ERROR:
						switch(event){
						case E_RESET:
							printf("Resetting the state...\n");
							state = S_OFFLINE;
							send_packet.opcode = O_SESSION_RESET;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
						break;
						case E_KILL:
							printf("killing this client and server...\n");
							send_packet.opcode = O_SESSION_KILL;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							kill_client = true;
						break;
						default:
							printf("We are in an error state."
							" Something went wrong\n");
						break;
						}
					break;
					default: 
						switch(event){
						case E_RESET:
							printf("Resetting the state...\n");
							state = S_OFFLINE;
							send_packet.opcode = O_SESSION_RESET;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
						break;
						case E_KILL:
							printf("killing this client and server...\n");
							send_packet.opcode = O_SESSION_KILL;
							ret = sendto(client_socket, &send_packet, 
							packet_size, 0, 
							(struct sockaddr *) &servaddr, serv_size);
							if(ret <= 0){
								perror("sendto() failed");
								return -1;
							}
							kill_client = true;
						break;
						default:
							printf("Error: waiting for ack from"
							" previous event\n");
						break;
						}
					break;
					}
				}
				else printf("Error: Unrecognized command format\n");
			}
			else printf("Error: Unrecognized command format\n");
		}
		else printf("Error: Unrecognized command format\n");
	}
	if(FD_ISSET(client_socket, &read_set)){
		ret = recvfrom(client_socket, &recv_packet, packet_size, 
		0, (struct sockaddr *) &servaddr, &serv_size);
		if(recv_packet.magic_bytes[0] == 'M' && 
		recv_packet.magic_bytes[1] == 'B'){ 
			switch(state){
			case S_ONLINE:
				switch(recv_packet.opcode){
				case O_FORWARD:
					printf("%s\n", recv_packet.pt.payload);
					send_packet.opcode = O_FORWARD_ACK;
					ret = sendto(client_socket, &send_packet, packet_size, 
					0, (struct sockaddr *) &servaddr, serv_size);
					if(ret <= 0){
						perror("sendto() failed");
						return -1;
					}
				break;
				case O_LOGIN_ERROR:
					printf("error#must_login_first\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;
				}
			break;
			case S_LOGIN_SENT:
				switch(recv_packet.opcode){
				case O_LOGIN_ACK_SUCCESS:
					printf("login_ack#successful\n");
					printf("This is the token: %u\n", recv_packet.token);
					send_packet.token = recv_packet.token;
					state = S_ONLINE;
				break;
				case O_LOGIN_ACK_FAILURE:
					printf("login_ack#failed\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;
				}
			break;
			case S_POST_SENT:
				switch(recv_packet.opcode){
				case O_POST_ACK:
					printf("post_ack#successful\n");
					state = S_ONLINE;
				break;
				case O_LOGIN_ERROR:
					printf("error#must_login_first\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;
				}
			break;
			case S_RETRIEVE_SENT:
				switch(recv_packet.opcode){
				case O_RETRIEVE_ACK:
					printf("%s\n", (recv_packet.pt.payload));
				break;
				case O_RETRIEVE_ACK_END:
					state = S_ONLINE;
				break;
				case O_RETRIEVE_ACK_FAILURE:
					printf("The retrieve number is larger than the amount"
					" present\n");
					state = S_ONLINE;
				break;
				case O_LOGIN_ERROR:
					printf("error#must_login_first\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;	
				}
			break;
			case S_LOGOUT_SENT:
				switch(recv_packet.opcode){
				case O_LOGOUT_ACK:
					printf("logout_ack#successful\n");
					state = S_OFFLINE;
				break;
				case O_LOGIN_ERROR:
					printf("error#must_login_first\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;
				}
			break;
			case S_SUBSCRIBE_SENT:
				switch(recv_packet.opcode){
				case O_SUBSCRIBE_ACK_SUCCESS:
					printf("subscribe_ack#successful\n");
					state = S_ONLINE;
				break;
				case O_SUBSCRIBE_ACK_FAILURE:
					printf("subscribe_ack#failed\n");
					state = S_ONLINE;
				break;
				case O_LOGIN_ERROR:
					printf("error#must_login_first\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;
				}
			break;
			case S_UNSUBSCRIBE_SENT:
				switch(recv_packet.opcode){
				case O_UNSUBSCRIBE_ACK_SUCCESS:
					printf("unsubscribe_ack#successful\n");
					state = S_ONLINE;
				break;
				case O_UNSUBSCRIBE_ACK_FAILURE:
					printf("unsubscribe_ack#failed\n");
					state = S_ONLINE;
				break;
				case O_LOGIN_ERROR:
					printf("error#must_login_first\n");
					state = S_OFFLINE;
				break;
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				default: state = S_ERROR; break;
				}
			break;
			case S_ERROR:
				printf("We are in an error state. Something went wrong\n");
				switch(recv_packet.opcode){
				case O_SESSION_RESET_ACK:
					printf("Session has been reset...\n");
					state = S_OFFLINE;
				break;
				}
			break;
			default:
				state = S_OFFLINE;
			break;
			}
		}
	} 
	} //end of while
	return 0;
} //end of main

